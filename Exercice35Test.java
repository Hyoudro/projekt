import org.junit.Test;
import static org.junit.Assert.assertTrue;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Exercice35Teste {

  @Test
  public void testExercice35Test() {
    ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    System.setOut(new PrintStream(outContent));

    Exercice35.main(null);

    assertTrue(outContent.toString().contains("s1 et s3 contiennent la même chaîne."));
    assertTrue(outContent.toString().contains("s1 et s2 contiennent la même chaîne en ignorant la casse,"));
  }
}
