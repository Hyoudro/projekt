public class Exercice35 {
  public static void main(String[] args) {
    String s1="Bonjour";
    String s2="bonjour";
    String s3="Bonjour";
    
    if(s1.equals(s2))
      System.out.println("s1 et s2 contiennent la même chaîne."); //si s1 et égal à s2 sa affiche
    if(s1.equals(s3))
      System.out.println("s1 et s3 contiennent la même chaîne."); //si s1 et égal à s3 sa affiche
    if(s1.equalsIgnoreCase(s2))  //si s1 et égal à s2 en ignorant la case sa affiche
      System.out.println("s1 et s2 contiennent la même chaîne en ignorant la casse,"
              + " c'est-à-dire la distinction entre une lettre minuscule et majuscule.");
  } 
}
